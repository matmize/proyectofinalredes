# Proyecto Final Redes y Comunicaciones #

Andres Felipe Jimenez y Mateo Montoya

### Sintesis Metodologia ###

Nosotros lo primero que hicimos fue hacer el subneteo de las IP las cuales se nos dio en las instrucciones del Proyecto Final dadas las IPS :
Red Campus : 190.29.0.0/16 
Mascara de Subred : 11111111.11111111.11111110.00000000/24
	Con un inccremento de 2,  	2^8 -2 = 254 hosts,		2^7 = 128 Subredes
	190.29.0.0 ---> 190.29.1.255
	190.29.2.0 ---> 190.29.3.255
	190.29.4.0 ---> 190.29.5.255
	.
	.
	.
	.
	190.29.254.0 ---> 190.29.255.255
	
Se Utilizaron : 
	
VLAN 30 : 190.29.30.0/24
VLAN 15 : 190.29.15.0/24
VLAN 10 : 190.29.10.0/24
VLAN 25 : 190.29.25.0/24
VLAN 99 : 190.29.99.0/24

#wan#

Se nos dio la Direccion IP 9.9.12.0/24

Siguiendo el proceso de subneteo llegamos a que :

Mascara de Subred : 11111111.11111111.111111111.111111.00/30

Conteniendo 64 Subredes, un incremento de 4 y 2 Ips Host

9.9.12.0 ----> 9.9.12.3
9.9.12.4  ----> 9.9.12.7
9.9.12.8----> 9.9.12.11
9.9.12.12----> 9.9.12.15
9.9.12.16---> 9.9.12.19
.
.
.
.
9.9.12.252 ----> 9.9.12.255

Se utilizaron : 


R1 - ISP : 9.9.12.0/30
ISP - CLOUD : 9.9.12.4/30


#LAN SERVIDORES #

Se nos dio la IP 209.165.211.0/24

Mascara:11111111.11111111.11111111.11100000/27

Conteniendo 8 Subredes, 30 IP Hosts, Y un incremento de 32 

209.165.211.0 ---> 209.165.211.31
209.165.211.32-> 209.165.211.63
209.165.211.64-> 209.165.211.95
209.165.211.96 -> 209.165.211.127
209.165.211.128-> 209.165.211.159
209.165.211.160-> 209.165.211.191
209.165.211.192-> 209.165.211.223
209.165.211.224-> 209.165.211.255

Se utilizo la Direccion IP :

Servidores :209.165.211.0/27

Se asignaron de la misma forma : 

Puerto : Fa0/2-0/6 a la vlan 99 nativa 
puerto : Fa0/7-0/10 a la Vlan 30 Biblioteca 
Puerto Fa0/11-0/15 a la Vlan 15 Estudiantes 
Puerto Fa0/16-0/20 a la Vlan 10 Cuerpo Docente 
Puerto Fa 0/21-0/24 a la Vlan 25 Servicio tecnico Servidor DHCP

Estas ultimas fueron completadas con la tabla de asignaciones de los requerimientos mínimos numero 1 de las instrucciones para el proyecto final. 


Siguiente al subneteo se realizo la topologia de red administrada en el documento de instrucciones dentro de Cisco Packet Tracer 
con una topologia de : 5 Switches en total, 1 WAN , 3 Servidores, 1 Home Gateway, 9 Nodos Terminales, y 13 Computadores.

A cada switch se le configuraron los puertos anteriormente mencionados y se crearon las vlan 30, 15, 25, 10 y 99 de las cuales se asignaron como acceso de los puertos correspondientes y la vlan 99 se declaró como vlan nativa en modo troncal para el paso de los datos entre las vlan.

El Router de salida para la red del campus se configuro con subinterfaces de acuerdo con las vlans creadas para así tener el método de enrutamiento “Router on a Stick”.

El servidor DHCP se configuro con varios pools de direcciones IP para que asignara automáticamente a cada host conectado a cada una de las VLAN de la red campus y además para la red de servidores. La red LAN mi casa no forma parte del pool del servidor ya que el HomeGateway tenia su propio protocolo DHCP para las conexiones tanto inalámbricas como por cable.

El HomeGateway no tenia CLI por lo tanto todas las configuraciones fueron mediante la interfaz grafica de cisco. Para que cada dispositivo IoT fuera identificado por el Gateway tenia que habilitarse la opción de homeGateway dentro de cada dispositivo y al asignar la IP del homeGateway automáticamente asignaba una dirección IP a cada dispositivo de la red. Para acceder a la interfaz de gestión de dispositivos basta con escribir la dirección IP del homeGateway en el web browser.

Ya con la topologia establecida se prosiguio a hacer cada una de las configuraciones: 

Primero se hizo la configuracion de cada uno de los switches con lo comandos aprendidos en clase, siguiente a esto se hizo la configuracion de los routers, despues se configuro el cloud, el siguiente paso fue configurar el Home Gateway
Terminadas ya estas configuraciones seguimos con la configuracion del lso servidores: WEB, DNS y DHCP.






Tabla de direccionamiento red LAN “Mi Casa “Inteligente””
Dispositivo			Interfaz			Dirección IP		Mascara
PCs					NIC					DHCP				DHCP

Wireless Host &
SmartPhones			Wireless NIC		DHCP				DHCP

Dispositivos IoT	Wireless NIC		DHCP				DHCP

HomeGateway
(DHCP)				LAN					192.168.12.1/24		255.255.255.0



Tabla de direccionamiento red “Campus”
Dispositivo			Interfaz			Dirección IP			Mascara
PCs VLANs			NIC					DHCP					DHCP
Servidor DHCP		NIC					190.29.25.2/24			255.255.255.0
R1			LAN		VLAN 30				190.29.30.1/24			255.255.255.0
					VLAN 15				190.29.15.1/24			255.255.255.0
					VLAN 10				190.29.10.1/24			255.255.255.0
					VLAN 25				190.29.25.1/24			255.255.255.0
					VLAN 99				190.29.99.0/24			255.255.255.0

Tabla de direccionamiento red WAN	
Dispositivo			Interfaz			Dirección IP			Mascara
R1					WAN					9.9.12.1/30				255.255.255.252

ISP					WAN					9.9.12.2/30				255.255.255.252
					WAN-CLOUD			9.9.12.5/30				255.255.255.252
					LAN					209.165.211.1/27		255.255.255.224

Tabla de direccionamiento Servidores
Dispositivo			Interfaz			Dirección IP			Mascara
Servidor Web		NIC					209.165.211.3/27		255.255.255.224
Servidor DNS		NIC					209.165.211.2/27		255.255.255.224


#Verificacion del Funcionamiento# 


Para la verificacion del funcionamiento del proyecto realizamos el comando ping de la pc0 a la pc4 para verificar la conexion de la misma vlan y la consola solto el siguiente mensaje: 
Packet Tracer PC Command Line 1.0



C:\>ping 190.29.25.7

Pinging 190.29.25.7 with 32 bytes of data:

Reply from 190.29.25.7: bytes=32 time<1ms TTL=128
Reply from 190.29.25.7: bytes=32 time<1ms TTL=128
Reply from 190.29.25.7: bytes=32 time<1ms TTL=128
Reply from 190.29.25.7: bytes=32 time<1ms TTL=128

Ping statistics for 190.29.25.7:
Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),

Approximate round trip times in milli-seconds:
Minimum = 0ms, Maximum = 0ms, Average = 0ms

ahora para verificar la conexion entre diferentes vlans usamos el mismo comando pero desde la pc 1 hasta la 7 usando las vlans 10 y 30 y cisco nos arrojo el siguiente mensaje : 

Packet Tracer PC Command Line 1.0

C:\>ping 190.29.30.4

Pinging 190.29.30.4 with 32 bytes of data:

Request timed out.
Reply from 190.29.30.4: bytes=32 time<1ms TTL=127
Reply from 190.29.30.4: bytes=32 time=1ms TTL=127
Reply from 190.29.30.4: bytes=32 time<1ms TTL=127

Ping statistics for 190.29.30.4:
Packets: Sent = 4, Received = 3, Lost = 1 (25% loss),

Approximate round trip times in milli-seconds:
Minimum = 0ms, Maximum = 1ms, Average = 0ms


Pudiendo asi verificar que ambas conexiones funcionan. 

#Analisis y Resultados #

1. El ordenador manda un solicitud que comunica con el servidor pero primero recibe un paquete por parte del servidor DNS que es el encargado de traducir la url a la direccion ip del servidor, generando ningun tipo de perdida de paquete y recuperando los
archivos html, css y jpg que conforma la pagina web.

2. A partir de la solicitud que se hace hacia el Home Gateway se manda un mensaje con el protocolo TCP a todos los componentes de la casa inteligente permitiendo asi realizar una accion a todos los componentes IoT conectados al Home Gateway

3. Desde el cliente se envia mensaje con el protocolo TCP y cuyando el servidor recibe el mensaje hace una respuesta con el mismo protocolo hacia el cliente. 


#Cpturas de Pantalla en Canva link#

https://www.canva.com/design/DAEvYgdfAqo/share/preview?token=w1bNlyjLVkLwCa3lugKdtg&role=EDITOR&utm_content=DAEvYgdfAqo&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton




#Link Video #

https://youtu.be/H6CADFQviNg




#Conclusiones #

En el proyecto realizado pudimos ver que al implementar los temas vistos en clase pudimos realizar el trabajo
sin mayores complicaciones, usando diferentes temas, tales como subneteo, protocolos, configuraciones de los dispositivos
y servidores, usando todos estos temas pudimos llevar a cabo el proyecto final del curso. 


#Recomendaciones#

Es un proyecto largo el cual requiere de ser empezado con tiempo de anticipacion, se necesitan saber muchos de los conceptos visto en clase sin embargo tambien se necesitan en ciertos casos hacer busqueda del material solicitado para la completacion
del proyecto ciertos elementos son mas dificiles de encontrar e implementar aunque el core del proyecto lo vimos en clase hay cosas que se escapan y generan que el proyecto sea un poco mas desafiante. 

#Referencias# 

Basic router configuration - cisco. (n.d.). Retrieved November 11, 2021, from https://www.cisco.com/en/US/docs/routers/access/800/850/software/configuration/guide/routconf.pdf. 
Cisco Press. VLAN Routing with Router 802.1Q Trunks &gt; IP Routing in the LAN | Cisco Press. (n.d.). Retrieved November 11, 2021, from https://www.ciscopress.com/articles/article.asp?p=2990405&amp;seqNum=2. 
ComputerNetworkingNotes. (2020, October 16). Configure DHCP server for multiple Vlans on the Switch. ComputerNetworkingNotes. Retrieved November 11, 2021, from https://www.computernetworkingnotes.com/ccna-study-guide/configure-dhcp-server-for-multiple-vlans-on-the-switch.html. 
Website templates, free website templates, free web templates, flash templates, website design. Website Templates, Free Website Templates, Free Web Templates, Flash Templates, Website Design. (n.d.). Retrieved November 11, 2021, from https://metamorphozis.com/. 
Cisco. (2017, February 6). Basic router configuration. Cisco. Retrieved November 11, 2021, from https://www.cisco.com/c/en/us/td/docs/routers/access/800M/software/800MSCG/routconf.html. 






